# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install forge
#
# You can edit this file again by typing:
#
#     spack edit forge
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack import *


class Forge(CMakePackage):
    """FORGE: I/O Forwarding Explorer."""

    homepage = "https://gitlab.com/jeanbez/forwarding-emulator"
    git      = "https://gitlab.com/jeanbez/forwarding-emulator.git"

    maintainers = ['jeanbez']

    version('latest', branch='master')

    depends_on('cmake')
    depends_on('libconfig')
    depends_on('openmpi@4.0.2')
    depends_on('gsl')
    depends_on('agios')
